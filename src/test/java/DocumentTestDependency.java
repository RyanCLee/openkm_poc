import com.openkm.SimulatedFunctions.MainClass;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DocumentTestDependency {

    @Test
    public void test() {
        // Arrange
        Boolean ExpectedResult = true; //logged in

        // Act
        MainClass test = new MainClass(); // MyClass is tested
        Boolean CheckResult = test.login("Admin", "secret", "test.pdf"); // Afhankelijk van resultaat van dependency

        String TestResult = test.checkSizeAndDocument();

        // assert statements
        Assert.assertEquals(CheckResult, ExpectedResult);
        Assert.assertEquals(TestResult, "PDF File is kleiner dan 50.0mb"); // Afhankelijk van resultaat van dependency
    }
}

