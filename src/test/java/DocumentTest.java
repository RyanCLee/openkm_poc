import com.openkm.SimulatedFunctions.Filesize;
import com.openkm.SimulatedFunctions.Credentials;
import com.openkm.SimulatedFunctions.MainClass;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;

public class DocumentTest {
    @InjectMocks
    private MainClass mainClass;

    @Mock
    private Credentials mockCredentials; // mock
    private Filesize mockFilesize = mock(Filesize.class); // mock

    @BeforeTest
    public void setup() {
        MockitoAnnotations.initMocks(this);  // or use mockito runner
    }


    @Test (priority=1)
    // Document size is smaller than max size and login is correct
    public void Test_DocumentSmallerThan() {
        Mockito.when(mockCredentials.login("ryan", "secret")).thenReturn(true); // Assign the mock object to be true
        Mockito.when(mockCredentials.validateDocument("Test.pdf")).thenReturn(true); // Assign the mock object to be true

        mainClass.login("ryan", "secret", "Test.pdf"); // To check the mock object and it will check against the mock, instead of the real implementation

        Mockito.when(mockFilesize.size()).thenReturn(50.1);
        Assert.assertEquals(mainClass.checkSizeAndDocument(),"Error: PDF bestand is groter dan 50.0mb");
    }


    @Test(priority=2)
    // Document is smaller than and login is wrong
    public void Test_DocumentSmallerThan_Login_Failed() {
        Mockito.when(mockCredentials.login("ryan", "secret")).thenReturn(true); // Assign the mock object to be true
        Mockito.when(mockCredentials.validateDocument("Test.pdf")).thenReturn(true); // Assign the mock object to be true

        mainClass.login("ryan", "fakepass", "Test.pdf"); // To check the mock object and it will check against the mock, instead of the real implementation

        Mockito.when(mockFilesize.size()).thenReturn(49.0);
        Assert.assertEquals(mainClass.checkSizeAndDocument(),"login failed");
    }

    @Test(priority=3)
    // Document is lager than and login is correct
    public void Test_DocumentKleinerDan() {
        Mockito.when(mockCredentials.login("ryan", "secret")).thenReturn(true); // Assign the mock object to be true
        Mockito.when(mockCredentials.validateDocument("Test.pdf")).thenReturn(true); // Assign the mock object to be true

        mainClass.login("ryan", "secret", "Test.pdf"); // To check the mock object and it will check against the mock, instead of the real implementation
        //   verify(mockCredentials).testMethod("ryan","secret");
//        mainClass.checkClass(); //assert

        Mockito.when(mockFilesize.size()).thenReturn(45.0);
        Assert.assertEquals(mainClass.checkSizeAndDocument(), "PDF bestand is kleiner dan 50.0mb");
//        Mockito.when(mockFilesize.size()).thenReturn(50.1);
//        Assert.assertEquals(mainClass.checkClass(),"PDF File is larger than 50.0");
    }






//    @Test
//    // Document is larger than and login is wrong
//    public void Test_DocumentLargerThan_Login_Failed() {
//        Mockito.when(mockCredentials.login("ryan", "secret")).thenReturn(false); // Assign the mock object to be true
//        Mockito.when(mockCredentials.validateDocument("Test.pdf")).thenReturn(true); // Assign the mock object to be true
//
//        mainClass.login("fakeusername", "fakepass", "Test.pdf"); // To check the mock object and it will check against the mock, instead of the real implementation
//
//        Mockito.when(mockFilesize.size()).thenReturn(50.1);
//        Assert.assertEquals(mainClass.checkSizeAndDocument(),"login failed");
//    }
}