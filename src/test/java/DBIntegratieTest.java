import org.testng.annotations.Test;

import java.sql.*;

public class DBIntegratieTest {
     String JDBC_DRIVER;
     String DB_URL;
     String USER;
     String PASS;
     Connection conn;
     Statement stmt;

    public void Initialize() throws SQLException, ClassNotFoundException {
        Class.forName("org.h2.Driver");
    JDBC_DRIVER = "org.h2.Driver"; //org.h2.Driver
    DB_URL = "jdbc:h2:~/TestDB";
    USER = "sa";
    PASS = "sa";
    conn = DriverManager.getConnection(DB_URL, USER, PASS);
    stmt = conn.createStatement();
    }

    @Test(priority=1)
    public void AddTable() throws SQLException, ClassNotFoundException {
        Initialize();

            System.out.println("Database: Toevoegen van tabel");

            String sql = "CREATE TABLE TestTable " +
                    "(gebruikersnaam VARCHAR not NULL, " +
                    " wachtwoord VARCHAR(255), " +
                    " email VARCHAR(255), " +
                    " mobiel INTEGER)";
             stmt.executeUpdate(sql);
             stmt.close();
        }

    @Test(priority=2)
    public void InsertData() throws SQLException, ClassNotFoundException {
        Initialize();
        System.out.println("Database: Toevoegen van gegevens");
        String sql = "INSERT INTO TestTable "
                + "VALUES ('Ryan', 'wachtwoord', 'ryan@test.com', '06000000')";
        stmt.close();
    }

    @Test(priority=3)
    public void UpdateData() throws SQLException, ClassNotFoundException {
        Initialize();
        System.out.println("Database: Updaten van gegevens");
        String sql = "UPDATE TestTable "
                + "SET gebruikersnaam = 'Test'";
        stmt.executeUpdate(sql);
        stmt.close();
    }

    @Test(priority=4)
    public void DeleteTable() throws SQLException, ClassNotFoundException {
        Initialize();
        System.out.println("Database: Verwijderen van gegevens");
        String sql = "DROP TABLE TestTable";
        stmt.executeUpdate(sql);
        stmt.close();
    }
}
