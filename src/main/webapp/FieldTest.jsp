<html>
<body>

<h1 id="myHeader">Gegevens validatie</h1>
<script>
    function ValidatieGebruikersnaam(){
        var username=document.getElementById("uname").value;
        if (username.length < 3) {
            document.getElementById("myHeader").innerHTML = "Gebruikersnaam moet minimaal 3 character bevatten!";
        } else if (username.length > 20) {
            document.getElementById("myHeader").innerHTML = "Gebruikersnaam mag niet meer dan 20 character bevatten!";
        }
        else if (username.indexOf("@") != -1 || username.indexOf("!") != -1 || username.indexOf("?") != -1 || username.indexOf("(") != -1 || username.indexOf(")") != -1 || username.indexOf("_") != -1 || username.indexOf("=") != -1 || username.indexOf("&") != -1 || username.indexOf("^") != -1 || username.indexOf("%") != -1 || username.indexOf("$") != -1 || username.indexOf("<") != -1 || username.indexOf(">") != -1 || username.indexOf("/") != -1 || username.indexOf("|") != -1 || username.indexOf("?") != -1 || username.indexOf("~") != -1 || username.indexOf("~") != -1 || username.indexOf("#") != -1)  {
            document.getElementById("myHeader").innerHTML = "Gebruikersnaam mag geen speciale character bevatten!";
        }
        else {
            document.getElementById("myHeader").innerHTML = "Correct!";
        }
    }

    function ValidatiePassword() {
        var password=document.getElementById("pass").value;
        if(password==""){
            document.getElementById("myHeader").innerHTML = "Voer je wachtwoord in!";
        }
        if (password.length < 5) {
            document.getElementById("myHeader").innerHTML = "Wachtwoord moet minimaal 5 character bevatten!";
        }
        else {
            document.getElementById("myHeader").innerHTML = "Correct!";
        }
    }


    function ValidatieVoornaam() {
        var voornaam=document.getElementById("fname").value;
        if (voornaam.length < 3) {
            document.getElementById("myHeader").innerHTML = "Voornaam moet minimaal 3 character bevatten!";
        }
        else if (voornaam.indexOf("0") != -1 || voornaam.indexOf("1") != -1 || voornaam.indexOf("2") != -1 || voornaam.indexOf("3") != -1 || voornaam.indexOf("4") != -1 || voornaam.indexOf("5") != -1 || voornaam.indexOf("6") != -1 || voornaam.indexOf("7") != -1 || voornaam.indexOf("8") != -1 || voornaam.indexOf("9") != -1) {
            document.getElementById("myHeader").innerHTML = "Voornaam mag geen cijfer bevatten!";
        } else if (voornaam.indexOf("@") != -1 || voornaam.indexOf("!") != -1 || voornaam.indexOf("?") != -1 || voornaam.indexOf("(") != -1 || voornaam.indexOf(")") != -1 || voornaam.indexOf("_") != -1 || voornaam.indexOf("=") != -1 || voornaam.indexOf("&") != -1 || voornaam.indexOf("^") != -1 || voornaam.indexOf("%") != -1 || voornaam.indexOf("$") != -1 || voornaam.indexOf("<") != -1 || voornaam.indexOf(">") != -1 || voornaam.indexOf("/") != -1 || voornaam.indexOf("|") != -1 || voornaam.indexOf("?") != -1 || voornaam.indexOf("~") != -1 || voornaam.indexOf("~") != -1 || voornaam.indexOf("#") != -1)  {
            document.getElementById("myHeader").innerHTML = "voornaam mag geen speciale character bevatten!";
        }
        else {
            document.getElementById("myHeader").innerHTML = "Correct";
        }
    }

    function ValidatieAchternaam() {
        var achternaam=document.getElementById("lname").value;
        if (achternaam.length < 3) {
            document.getElementById("myHeader").innerHTML = "Achternaam moet minimaal 3 character bevatten!";
        }
        else if (achternaam.indexOf("0") != -1 || achternaam.indexOf("1") != -1 || achternaam.indexOf("2") != -1 || achternaam.indexOf("3") != -1 || achternaam.indexOf("4") != -1 || achternaam.indexOf("5") != -1 || achternaam.indexOf("6") != -1 || achternaam.indexOf("7") != -1 || achternaam.indexOf("8") != -1 || achternaam.indexOf("9") != -1) {
            document.getElementById("myHeader").innerHTML = "Achternaam mag geen cijfer bevatten!";
        } else if (achternaam.indexOf("@") != -1 || achternaam.indexOf("!") != -1 || achternaam.indexOf("?") != -1 || achternaam.indexOf("(") != -1 || achternaam.indexOf(")") != -1 || achternaam.indexOf("_") != -1 || achternaam.indexOf("=") != -1 || achternaam.indexOf("&") != -1 || achternaam.indexOf("^") != -1 || achternaam.indexOf("%") != -1 || achternaam.indexOf("$") != -1 || achternaam.indexOf("<") != -1 || achternaam.indexOf(">") != -1 || achternaam.indexOf("/") != -1 || achternaam.indexOf("|") != -1 || achternaam.indexOf("?") != -1 || achternaam.indexOf("~") != -1 || achternaam.indexOf("~") != -1 || achternaam.indexOf("#") != -1)  {
            document.getElementById("myHeader").innerHTML = "Achternaam mag geen speciale character bevatten!";
        }
        else {
            document.getElementById("myHeader").innerHTML = "Correct";
        }
    }

    function ValidatieEmail() {
        var email=document.getElementById("email").value;
        var atpos=email.indexOf("@");
        var dotpos=email.lastIndexOf(".");

        if (email.indexOf("!") != -1 || email.indexOf("`") != -1 || email.indexOf("?") != -1 || email.indexOf("(") != -1 || email.indexOf(")") != -1 || email.indexOf("=") != -1 || email.indexOf("&") != -1 || email.indexOf("^") != -1 || email.indexOf("%") != -1 || email.indexOf("$") != -1 || email.indexOf("<") != -1 || email.indexOf(">") != -1 || email.indexOf("/") != -1 || email.indexOf("|") != -1 || email.indexOf("?") != -1 || email.indexOf("~") != -1 || email.indexOf("~") != -1 || email.indexOf("#") != -1)  {
            document.getElementById("myHeader").innerHTML = "Email adres is niet toegestaan!";
        }
        else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
            document.getElementById("myHeader").innerHTML = "Email adres is niet toegestaan!";
        }
        else {
            document.getElementById("myHeader").innerHTML = "Correct";
        }
    }
</script>



<center>
	<table border="1" width="30%" cellpadding="5">
		<thead>
		<tr>
			<th colspan="2">Gegevens invullen</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>Gebruikersnaam</td>
			<td><input type="text" id="uname" value="" /></td>
			<td><button onclick="ValidatieGebruikersnaam()">Validate</button></td>
		</tr>
		<tr>
			<td>Wachtwoord</td>
			<td><input type="password" id="pass" value="" /></td>
			<td><button onclick="ValidatiePassword()">Validate</button></td>
		</tr>
		<tr>
			<td>Voornaam</td>
			<td><input type="text" id="fname" value="" /></td>
			<td><button onclick="ValidatieVoornaam()">Validate</button></td>
		</tr>
		<tr>
			<td>Achternaam</td>
			<td><input type="text" id="lname" value="" /></td>
			<td><button onclick="ValidatieAchternaam()">Validate</button></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input type="text" id="email" value="" /></td>
			<td><button onclick="ValidatieEmail()">Validate</button></td>
		</tr>
		<tr>
			<td><input type="submit" value="Submit" /></td>
		</tr>
		</tbody>
	</table>
</center>



</body>
</html>