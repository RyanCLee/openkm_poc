package com.openkm.SimulatedFunctions;

public class MainClass  {
    boolean login;
    boolean pdf;

    private Credentials credentials = new Credentials();
    private Filesize custominformation = new Filesize();


    // Credentials ophalen
    public boolean login(String username, String password, String document) {
        if (this.credentials.login(username, password) == true && this.credentials.validateDocument(document) == true) { // Dependency
            System.out.println("Ingelogd met de juiste gebruikersnaam en wachtwoord");
            login = true;
            pdf = true;
        }
        else {
            System.out.println("Verkeerde gebruikersnaam en/of wachtwoord");
            login = false;
            pdf = false;
        }
        return login;
    }

    // Document validation
    public String checkSizeAndDocument() {
        String validate = "Error";
        if (login != false) {
            if (this.custominformation.size() < 50.0 && login == true) {
                validate = "PDF bestand is kleiner dan 50.0mb";
                System.out.println("PDF bestand is kleiner dan 50.0mb");
            } else if (this.custominformation.size() < 50.0 && login == false) {
                validate = "Access denied";
                System.out.println("Access denied");
            } else if (this.custominformation.size() > 50.0 && login == true || login == false) {
                validate = "Error: PDF bestand is groter dan 50.0mb";
                System.out.println("Bestand is groter dan 50.0mb");
            }
        }
        else {
            validate = "login failed";
        }
            return validate;
    }
}




