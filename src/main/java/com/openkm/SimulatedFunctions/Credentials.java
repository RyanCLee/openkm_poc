package com.openkm.SimulatedFunctions;

public class Credentials {
    public boolean login(String inputUsername, String inputPassword) {
        if (inputUsername == "Admin" && inputPassword == "secret") {
            System.out.println("Wrong username or password");
            return true;
        } else {
            System.out.println("Right username or password");
            return false;
        }
    }

    public boolean validateDocument(String document) {
        String CheckExtension = document.substring(document.lastIndexOf("."), document.length());
        if (CheckExtension.equals(".pdf")) {
            System.out.println("document is pdf");
        }
        return true;
    }
}