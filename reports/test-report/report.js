$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("PermissionCase.feature");
formatter.feature({
  "line": 1,
  "name": "",
  "description": "",
  "id": "",
  "keyword": "Feature"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "url \u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
  "keyword": "* "
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 1317796181,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Login and get Token",
  "description": "",
  "id": ";login-and-get-token",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "request",
  "keyword": "Given ",
  "doc_string": {
    "content_type": "",
    "line": 7,
    "value": "\u003csoapenv:Envelope xmlns:soapenv\u003d\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws\u003d\"http://ws.openkm.com\"\u003e\n   \u003csoapenv:Header/\u003e\n   \u003csoapenv:Body\u003e\n      \u003cws:login\u003e\n         \u003c!--Optional:--\u003e\n         \u003cuser\u003eTestUser\u003c/user\u003e\n         \u003c!--Optional:--\u003e\n         \u003cpassword\u003eTestUser\u003c/password\u003e\n      \u003c/ws:login\u003e\n   \u003c/soapenv:Body\u003e\n\u003c/soapenv:Envelope\u003e"
  }
});
formatter.step({
  "line": 20,
  "name": "soap action \u0027http://ws.openkm.com/OKMAuth/login\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "status 200",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "def last \u003d //return",
  "keyword": "* "
});
formatter.step({
  "line": 24,
  "name": "print last + \u0027######################TEST##########################\u0027",
  "keyword": "* "
});
formatter.match({
  "location": "StepDefs.requestDocString(String)"
});
formatter.result({
  "duration": 7528180,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": " \u0027http://ws.openkm.com/OKMAuth/login\u0027",
      "offset": 11
    }
  ],
  "location": "StepDefs.soapAction(String)"
});
formatter.result({
  "duration": 2529594974,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 7
    }
  ],
  "location": "StepDefs.status(int)"
});
formatter.result({
  "duration": 1317799,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "last",
      "offset": 4
    },
    {
      "val": "//return",
      "offset": 11
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 17240235,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "last + \u0027######################TEST##########################\u0027",
      "offset": 6
    }
  ],
  "location": "StepDefs.print(String\u003e)"
});
formatter.result({
  "duration": 46397360,
  "status": "passed"
});
});